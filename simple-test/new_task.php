<?php

require_once __DIR__ . '/vendor/autoload.php';
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

$connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
$channel = $connection->channel();

$data = implode(' ', array_slice($argv, 1));
if(empty($data)) $data = "Hello World!";
$k = 0;
while($k < 1000){
	$msg = new AMQPMessage($data . " " . $k,
                        array('delivery_mode' => 2) # make message persistent
      );

	$channel->basic_publish($msg, '', 'task_queue');
echo " [x] Sent ", $k++, "\n";
}
